import React from "react";
import { Tooltip } from "@chakra-ui/react";
import styles from "./Timeline.module.css";
import { DAY_TIME_IN_SECONDS, flightOnAirPercent } from "../../utils";

function ScheduleTime({ flight }) {
  const percent = flightOnAirPercent(flight.arrivaltime, flight.departuretime);
  const leftPosition = (100 * flight.departuretime) / DAY_TIME_IN_SECONDS;

  return (
    <Tooltip
      label={`${flight.ident}: ${flight.readable_departure} - ${flight.readable_arrival}`}
    >
      <div
        className={styles.schedule}
        style={{ minWidth: `${percent}%`, left: `${leftPosition}%` }}
      ></div>
    </Tooltip>
  );
}

export default ScheduleTime;
