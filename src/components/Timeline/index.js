import React from "react";
import styles from "./Timeline.module.css";
import { useRotation } from "../../context/RotationContext";
import ScheduleTime from "./ScheduleTime";
import TurnAroundTime from "./TurnAroundTime";

function Timeline() {
  const { rotationList } = useRotation();

  if (rotationList.length === 0) {
    return null;
  }

  return (
    <div className={styles.timeline}>
      {rotationList.map((flight) => {
        return (
          <div key={flight.ident}>
            <ScheduleTime flight={flight} />
            <TurnAroundTime flight={flight} />
          </div>
        );
      })}
    </div>
  );
}

export default Timeline;
