import React from "react";
import { Tooltip } from "@chakra-ui/react";
import styles from "./Timeline.module.css";
import { DAY_TIME_IN_SECONDS, TURNAROUND_TIME } from "../../utils";

function TurnAroundTime({ flight }) {
  const percent = (100 * TURNAROUND_TIME) / DAY_TIME_IN_SECONDS;
  const leftPosition = (100 * flight.arrivaltime) / DAY_TIME_IN_SECONDS;

  return (
    <Tooltip label={`Turnaround flight: ${flight.ident}`}>
      <div
        className={styles.turnAround}
        style={{ minWidth: `${percent}%`, left: `${leftPosition}%` }}
      ></div>
    </Tooltip>
  );
}

export default TurnAroundTime;
