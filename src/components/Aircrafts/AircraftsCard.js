import React from "react";
import { chakra, Box } from "@chakra-ui/react";

function AircraftsCard({ aircraft, rotationOnAirTimePercent, ...rest }) {
  return (
    <Box
      bg={"white"}
      shadow="md"
      rounded="lg"
      mx="auto"
      cursor="pointer"
      px="10"
      {...rest}
    >
      <Box py={5} textAlign="center">
        <chakra.p
          display="block"
          fontSize="xl"
          color={"gray.800"}
          fontWeight="bold"
        >
          {aircraft.ident}
        </chakra.p>
        <chakra.span fontSize="sm" color={"gray.700"}>
          ({Math.round(rotationOnAirTimePercent)}%)
        </chakra.span>
      </Box>
    </Box>
  );
}

export default AircraftsCard;
