import React from "react";
import AircraftsCard from "./AircraftsCard";
import { useRotation } from "../../context/RotationContext";
import { Heading } from "@chakra-ui/react";
import { useQuery } from "react-query";

export default function Aircrafts({ setCurrentAircraft, currentAircraft }) {
  const { isLoading, error, data } = useQuery("aircraftData", () =>
    fetch("https://infinite-dawn-93085.herokuapp.com/aircrafts").then((res) =>
      res.json()
    )
  );

  const { rotationOnAirTimePercent, cleanRotation } = useRotation();

  if (isLoading) return "Loading...";

  if (error) return "An error has occurred: " + error.message;

  const aircraftsData = data.data;

  const handleOnClick = (aircraft) => {
    if (currentAircraft?.ident !== aircraft.ident) {
      setCurrentAircraft(aircraft);
      cleanRotation();
    }
  };

  return (
    <div>
      <Heading textAlign="center" mb="6">
        Aircrafts
      </Heading>
      {aircraftsData.map((aircraft) => {
        return (
          <AircraftsCard
            aircraft={aircraft}
            rotationOnAirTimePercent={rotationOnAirTimePercent}
            key={aircraft.ident}
            onClick={() => {
              handleOnClick(aircraft);
            }}
          />
        );
      })}
    </div>
  );
}
