import { Box, Text, Stack, Button } from "@chakra-ui/react";
import RotationCard from "./RotationCard";
import { useRotation } from "../../context/RotationContext";

export default function RotationList({ aircraft }) {
  const { rotationList, removeLastFlight } = useRotation();

  if (rotationList.length === 0) {
    return <Text textAlign="center">No flights on the rotation</Text>;
  }

  return (
    <Box>
      {rotationList.map((flight) => {
        return <RotationCard flight={flight} key={flight.ident} />;
      })}

      <Stack direction="row" spacing={4} justifyContent="flex-end" mb={4}>
        <Button
          colorScheme="teal"
          variant="solid"
          onClick={() => {
            removeLastFlight();
          }}
        >
          Remove last flight
        </Button>
      </Stack>
    </Box>
  );
}
