import React from "react";
import { Heading } from "@chakra-ui/react";
import RotationList from "./RotationList";

export default function Rotation({ aircraft }) {
  return (
    <div>
      <Heading textAlign="center" mb="6">
        Rotation {aircraft.ident}
      </Heading>
      <RotationList />
    </div>
  );
}
