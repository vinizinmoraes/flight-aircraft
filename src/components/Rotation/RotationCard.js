import React from "react";
import { Box, Flex, Text } from "@chakra-ui/react";

function RotationCard({ flight }) {
  return (
    <Box bg={"white"} shadow="md" rounded="lg" mb="4" px="10">
      <Box py={5}>
        <Text>Flight: {flight.ident}</Text>
        <Flex justifyContent="space-between">
          <Box>
            <Text>{flight.origin}</Text>
            <Text>{flight.readable_departure}</Text>
          </Box>
          <Box>
            <Text>{flight.destination}</Text>
            <Text>{flight.readable_arrival}</Text>
          </Box>
        </Flex>
      </Box>
    </Box>
  );
}

export default RotationCard;
