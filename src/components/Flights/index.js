import React from "react";
import { Heading, Box, Text } from "@chakra-ui/react";
import FlightsList from "./FlightsList";

export default function Flights() {
  return (
    <div>
      <Heading textAlign="center" mb="6">
        Flights
      </Heading>
      <FlightsList />
    </div>
  );
}
