import React from "react";
import { Box, Flex } from "@chakra-ui/react";
import { useRotation } from "../../context/RotationContext";

function FlightsCard({ flight }) {
  const { addFlight, lastFlight } = useRotation();

  return (
    <Box
      bg={"white"}
      shadow="md"
      rounded="lg"
      cursor="pointer"
      mb="4"
      px="10"
      onClick={() => {
        addFlight(flight);
      }}
    >
      <Box py={5} textAlign="center">
        <p>{flight.ident}</p>
        <Flex justifyContent="space-between">
          <Box>
            <p>{flight.origin}</p>
            <p>{flight.readable_departure}</p>
          </Box>
          <Box>
            <p>{flight.destination}</p>
            <p>{flight.readable_arrival}</p>
          </Box>
        </Flex>
      </Box>
    </Box>
  );
}

export default FlightsCard;
