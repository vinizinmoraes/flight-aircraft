import React from "react";
import { Box, Text } from "@chakra-ui/react";
import flightsData from "../../data/flights.json";
import FlightsCard from "./FlightsCard";
import { useRotation } from "../../context/RotationContext";

function FlightsList() {
  const { nextPossibleFlightTime, nextPossibleOrigin } = useRotation();

  const possibleFlights = flightsData.filter((flight) => {
    if (flight.departuretime <= nextPossibleFlightTime) {
      return false;
    }
    if (nextPossibleOrigin && flight.origin !== nextPossibleOrigin) {
      return false;
    }
    return true;
  });

  if (possibleFlights.length === 0) {
    return <Text textAlign="center">No more possible flights</Text>;
  }

  return (
    <Box>
      {possibleFlights.map((flight) => {
        return <FlightsCard flight={flight} key={flight.ident} />;
      })}
    </Box>
  );
}

export default FlightsList;
