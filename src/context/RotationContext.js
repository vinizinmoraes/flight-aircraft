import React from "react";
import { TURNAROUND_TIME, flightOnAirPercent } from "../utils";

const RotationContext = React.createContext(null);

const useRotation = () => {
  const [rotation, setRotation] = React.useContext(RotationContext);

  const nextPossibleFlightTime =
    rotation.length > 0
      ? rotation[rotation.length - 1].arrivaltime + TURNAROUND_TIME
      : 0;

  const nextPossibleOrigin =
    rotation.length > 0 ? rotation[rotation.length - 1].destination : null;

  const rotationOnAirTimePercent = rotation.reduce((acc, flight) => {
    return acc + flightOnAirPercent(flight.arrivaltime, flight.departuretime);
  }, 0);

  const addFlight = (value) => {
    setRotation((prevState) => [...prevState, value]);
  };

  const removeFlight = (value) => {
    setRotation((prevState) => {
      return prevState.filter((flight) => {
        return flight.ident !== value;
      });
    });
  };

  const removeLastFlight = () => {
    setRotation((prevState) => {
      return prevState.filter((flight) => {
        return flight.ident !== prevState[prevState.length - 1].ident;
      });
    });
  };

  const cleanRotation = () => {
    setRotation([]);
  };

  return {
    rotationList: rotation,
    removeLastFlight,
    addFlight,
    cleanRotation,
    nextPossibleFlightTime,
    nextPossibleOrigin,
    rotationOnAirTimePercent,
  };
};

const RotationProvider = ({ value, children }) => {
  const [rotation, setRotation] = React.useState([]);

  return (
    <RotationContext.Provider value={[rotation, setRotation]}>
      {children}
    </RotationContext.Provider>
  );
};

export { RotationProvider, useRotation };
