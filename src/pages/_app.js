import "../styles/globals.css";
import { RotationProvider } from "../context/RotationContext";
import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();

const theme = {
  styles: {
    global: {
      "html, body": {
        backgroundColor: "gray.100",
      },
    },
  },
};

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider theme={extendTheme(theme)}>
      <QueryClientProvider client={queryClient}>
        <RotationProvider>
          <Component {...pageProps} />
        </RotationProvider>
      </QueryClientProvider>
    </ChakraProvider>
  );
}

export default MyApp;
