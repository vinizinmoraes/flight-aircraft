import { DAY_TIME_IN_SECONDS } from "./config";

export function flightOnAirPercent(arrivaltime, departuretime) {
  const onAirTime = arrivaltime - departuretime;
  return (100 * onAirTime) / DAY_TIME_IN_SECONDS;
}
