This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Assumptions

- I created a tooltip to show the info on the timeline, but I didn't have any better UX Idea.
- The turnaround time is always 20 minutes, but to show on the timeline I wasn't sure if its to be purple just the 20 minutes or the whole space between the flights, so I follow the rule about being 20 minutes. In the timeline you will see that is a little purple bar with the same width after each flight.
- I didnt use a API for the flights to waste less time on it. If its a production code I would go with one of this two options:
  - 1 - Ask if its possible to filter for origin of flights and by time (so I will only show the results that has a time that matches the last flight + 20 minutes and the airport is equal to where the airplane is)
  - 2 - If doing that on the API is not possible we can do something on the frontend, we can do a infinite scroll or a button to load more flights, and each time we load more flights from the page and just show the results that follow our rules.
- Should create a unit test using react testing library and jest to ensure that each interaction from the user is working properly, and if every condition that come from a API like loading and error is passing. If its a production code sometime a end to end test with cypress is something to consider.

## Conclusion

I think that is it. I enjoy the challenge, and I hope to be working with you next time!
